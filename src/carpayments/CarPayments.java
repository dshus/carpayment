package carpayments;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
public class CarPayments {
	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		List<Double> vals = new ArrayList<Double>();
		double currVal = 0.0;
		do {
                    System.out.print("Enter the amount paid: ");
                    if (sc.hasNext()) {
                        if (sc.hasNextDouble()) {
                            currVal = sc.nextDouble();
                            if (currVal < 0)
                                continue; //Don't allow -1 into vals.
                            vals.add(currVal);
                        } else
                            System.err.println(sc.next() + " is not a valid input. It will not be counted.");
                    }
		} while (currVal >= 0); //End when a negative number is entered.
                if (vals.isEmpty()) { //Troll check
                    System.err.println("No inputs were processed.");
                    return;
                }
		System.out.println("Calculating answers..."); //Here we go...
                HashMap<Double, Integer> modes = new HashMap<Double, Integer>();
                double min = vals.get(0); //Smallest value
                double max = vals.get(0); //Largest value
                double total = 0; //Sum (used in average)
                for (double now : vals) {
                    if (now < min)
                        min = now; //New smallest
                    if (now > max)
                        max = now; //New Largest
                    total += now; //Add to total
                    if (!modes.containsKey(now))            //If the price hasn't been seen before
                        modes.put(now, 1);                  //add it to the HashMap.
                    else                                    //Otherwise,
                        modes.put(now, modes.get(now) + 1); //it must have been seen before.  Increment the instance counter.
                }
                int nMode = 0; //Largest mode
                List<Double> mode = new ArrayList<Double>(); //Array of modes
                Set<Double> keys = modes.keySet(); //All unique values
                for (double currentKey : keys) {
                    if (modes.get(currentKey) > nMode) {       //If newest instance count is larger than the former,
                        mode.clear();                          //clear the list of previous modes (this one appears more often),
                        nMode = modes.get(currentKey);         //set the new highest mode,
                        mode.add(currentKey);                  //and add the new mode to the list of modes.  It's the only one in the list right now.
                    } else if (modes.get(currentKey) == nMode) //Otherwise, if the instance count is the same as the current mode list,
                        mode.add(currentKey);                  //add to the list without overwriting.
                }
                //Output answers
                System.out.println("Done calculating!");
                System.out.println("Smallest Value: " + min);
                System.out.println("Largest Value: " + max);
                System.out.println("Total (not required): " + total);
                System.out.println("Average: " + total/vals.size());
                System.out.println("Modes: " + mode.toString());
	}
}
